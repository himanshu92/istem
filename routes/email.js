var express = require('express');
var router = express.Router();
var emailController = require("../controllers/emailController");

/* GET home page. */
router.post('/', emailController.email);

module.exports = router;
