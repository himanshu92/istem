$(document).ready(function() {
  $('#summernote').summernote();
});

$('#xhrSave').submit(function(event) {

  event.preventDefault();
  var url = $(this).attr('action');
  var jsonObj = toJSON(this);
  jsonObj['userType'] = $('input[name=userType]:checked').val();

  $.ajax({
    url: url,
    type: 'PUT',
    data: JSON.stringify(jsonObj),
    datatype: 'json',
    contentType:"application/json; charset=utf-8",
    success: function(result) {
      // Do something with the result
    }
  });
});
function toJSON( form ) {
  var obj = {};
  var elements = form.querySelectorAll( "input, select, textarea" );
  for( var i = 0; i < elements.length; ++i ) {
    var element = elements[i];
    var name = element.name;
    var value = element.value;
    if( name ) {
      obj[ name ] = value;
    }
  }
  return obj;
  //return JSON.stringify( obj );
}
