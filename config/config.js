module.exports = {
    "development": {
        "username": process.env.SQL_DB_USER,
        "password": process.env.SQL_DB_PASSWORD,
        "database": process.env.SQL_DB_DATABASE,
        "host": process.env.SQL_DB_HOST,
        "dialect": "postgres",
        "pool": {
            "max": 10,
            "min": 0,
            "idle": 1000
        },
        "logging": console.log,
        "use_env_variable": false
    },
    "test": {
        "username": process.env.SQL_DB_USER,
        "password": process.env.SQL_DB_PASSWORD,
        "database": process.env.SQL_DB_DATABASE,
        "host": process.env.SQL_DB_HOST,
        "dialect": process.env.SQL_DB_DIALECT,
        "pool": {
            "max": 5,
            "min": 0,
            "idle": 1000
        },
        "logging": console.log,
        "use_env_variable": false
    },
    "production": {
        "username": process.env.SQL_DB_USER,
        "password": process.env.SQL_DB_PASSWORD,
        "database": process.env.SQL_DB_DATABASE,
        "host": process.env.SQL_DB_HOST,
        "dialect": process.env.SQL_DB_DIALECT,
        "pool": {
            "max": 10,
            "min": 0,
            "idle": 1000
        },
        "use_env_variable": false
    }, 
    "prod": {
        "username": process.env.SQL_DB_USER,
        "password": process.env.SQL_DB_PASSWORD,
        "database": process.env.SQL_DB_DATABASE,
        "host": process.env.SQL_DB_HOST,
        "dialect": process.env.SQL_DB_DIALECT,
        "pool": {
            "max": 10,
            "min": 0,
            "idle": 1000
        },
        "use_env_variable": false
    }
};
