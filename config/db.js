const mongoose = require('mongoose');

let isConnected;

mongoose.set('debug', true);

mongoose.Promise = global.Promise;

// Build the connection string
const dbURI =
  process.env == 'development'
    ? process.env.MONGODB_URI
    : process.env.MONGOLAB_URI;

module.exports = () => {
  if (isConnected) {
    return Promise.resolve();
  }
  return mongoose
    .connect(dbURI, { useNewUrlParser: true })
    .then(db => {
      console.log(dbURI);
      console.log('MongoDB Connected');
      isConnected = db.connections[0].readyState;

      if (process.env.IS_OFFLINE) {
        mongoose.set('debug', true);
      }
      return isConnected;
    })
    .catch(error => {
      console.log('DB Error: ', error);
      throw error;
      // next();
      // return Promise.reject();
    });
};
