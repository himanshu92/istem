'use strict';

const mongoose = require('mongoose');
const userSchema = require('./user');

const emailSchema = new mongoose.Schema(
  {
    subject: {
      type: String,
      required: true
    },
    body: {
      type: String,
      required: true
    },
    status: {
      type: Number
    },
    user: userSchema
  },
  {
    id: true,
    timestamps: true,
    strict: false
  }
);

var Email = mongoose.model('Email', emailSchema);
module.exports = Email;
