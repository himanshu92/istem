const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true
    },
    password: String,
    organisation: String,
    userType: Number
  },
  {
    id: true,
    timestamps: true,
    strict: false,
    hooks: {
      beforeCreate: function(user) {
        console.log(user.password);
        if (user.password) {
          user.password = bcrypt.hashSync(
            user.password,
            bcrypt.genSaltSync(8),
            null
          );
          return;
        }
      },
      beforeUpdate: function(user) {
        console.log(user.password);
        console.log(user.changed('password'));
        if (user.changed('password')) {
          user.password = bcrypt.hashSync(
            user.password,
            bcrypt.genSaltSync(8),
            null
          );
          console.log(user);
          return;
        }
      }
    }
  }
);

const encryptPassword = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
userSchema.pre('save', function(next) {
  console.log(this);
  if (this.password) {
    this.password = encryptPassword(this.password);
  }
  next();
});

userSchema.pre('update', function(next) {
  if (this.isModified('password')) {
    // If the pw has been modified, then encrypt it again
    this.password = encryptPassword(this.password);
  }
  next();
});
userSchema.pre('findOneAndUpdate', function(next) {
  if (this._update.password) {
    // If the pw has been modified, then encrypt it again
    this._update.password = encryptPassword(this._update.password);
  }
  next();
});
// checking if password is valid
userSchema.methods.validPassword = function(password) {
  if (this.password) {
    return bcrypt.compareSync(password, this.password);
  } else {
    return false;
  }
};

var User = mongoose.model('User', userSchema);
module.exports = User;
/*
User.createMapping((err, mapping) => {
  if (err) {
    console.log("error creating mapping (you can safely ignore this)");
    console.log(err);
  } else {
    console.log("mapping created!");
    console.log(mapping);
  }
}); */
