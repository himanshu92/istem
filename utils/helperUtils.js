/**
 * Created by Himanshu on 05/09/19.
 */

var mailer = require('nodemailer');

var xoauth2 = require('xoauth2');

var transporter = mailer.createTransport({
  service: 'Gmail',
  auth: {
    xoauth2: xoauth2.createXOAuth2Generator({
      user: 'jainjainhimanshu@gmail.com',
      clientId:
        '673292150278-6bchod2lqujo8306e2rpjlki4pis2qrk.apps.googleusercontent.com',
      clientSecret: 'asSHiBnnJMhf05vlUjdW27D8',
      refreshToken: '1/YX5wNwmOPhEdUyvSM3qrdfrYkApgKhnQ69RfjqSI1M4'
      //  //accessToken: '{cached access token}'
    })
    //user: config.email.USER, // Your email id
    //pass: config.email.PASSWORD // Your password
  }
});

exports.sendMail = function(email_data, to_list) {
  var options = {
    from: 'jainjainhimanshu@gmail.com',
    to: to_list,
    bcc: 'kartik@inclusivestem.org',
    subject: email_data.subject,
    html: email_data.html
  };

  transporter.sendMail(options, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Message sent: ' + info.response);
    }
  });
};
