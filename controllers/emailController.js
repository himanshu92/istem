/**
 * GET /*
 * Home page.
 */

'use strict';
const User = require('../models/user');
const mailer = require('../utils/helperUtils');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  email: function(req, res, next) {
    User.find({ userType: req.body.userType }).then(async users => {
      var promiseArr = [];
      try {
        for (var index in users) {
          await mailer.sendMail(
            { subject: req.body.subject, html: req.body.body },
            users[index].email,
            null
          );
        }
        return res.render('index', {
          title: 'Istem',
          user: req.user,
          message: promiseArr.length + ' Emails Sent'
        });
      } catch (err) {
        return res.render('index', {
          title: 'Istem',
          user: req.user,
          message: 'Email Failure'
        });
      }
    });
  }
};

var sendEmailPromise = function(message) {
  return new Promise(function(resolve, reject) {
    sendEmail(message)
      .then(function(info) {
        console.log(info);
        resolve(info);
      }) // if successful
      .catch(function(err) {
        console.log('got error');
        reject(err);
      });
  });
};
