/**
 * GET /*
 * Home page.
 */

'use strict';
const User = require('../models/user');

/**
 *
 * @type {{index: Function}}
 */
module.exports = {
  register: function(req, res, next) {
    console.log(req.body);
    User.create(req.body).then(user => {
      return res.render('index', {
        title: 'Istem',
        user: user,
        message: 'Hello ' + user.firstName
      });
    });
  },
  update: function(req, res, next) {
    User.findOneAndUpdate({ _id: req.params.id }, req.body, {
      upsert: true,
      new: true
    })
      .then(userResponse => {
        req.user = userResponse;
        return res.render('index', {
          title: 'Istem',
          user: userResponse,
          message: 'Hello ' + userResponse.firstName
        });
      })
      .catch(err => {
        res
          .json(standardResponse({ err }, res.statusCode, 'Error'))
          .status(500);
      });
  }
};
