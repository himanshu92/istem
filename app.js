var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var dotenv = require('dotenv');
var passport = require('passport');
var app = express();

dotenv.load({ path: '.env' });

require('./config/passport');

require('./config/db')().catch(err => {
  throw err;
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  session({
    key: 'istem_sid',
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: 24 * 60 * 60 * 60000 },
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(
  passport.session({
    key: 'istem_sid',
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: 24 * 60 * 60 * 60000 },
    resave: false,
    saveUninitialized: false
  })
);

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/login');
}

app.all('*', function(req, res, next) {
  if (
    req.path === '/login' ||
    req.path === '/user/login' ||
    req.path === '/user/register'
  )
    next();
  else ensureAuthenticated(req, res, next);
});

var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
var emailRouter = require('./routes/email');

app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/email', emailRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
